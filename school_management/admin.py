from django.contrib import admin

from school_management.models import Profile, Student, Course, Teacher

# Register your models here.
admin.site.register(Profile)
admin.site.register(Course)
admin.site.register(Student)
admin.site.register(Teacher)

