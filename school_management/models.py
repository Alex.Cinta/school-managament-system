from django.contrib.auth.models import User, AbstractUser
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import CASCADE
from django.utils import timezone

classes = [('one', 'one'), ('two', 'two'), ('three', 'three'), ('four', 'four')]


class Profile(models.Model):
    ROLE_CHOICES = (
        ('Admin', 'Admin'),
        ('Teacher', 'Teacher'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username


class Student(models.Model):
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )
    firstname = models.CharField(max_length=255)
    surname = models.CharField(max_length=255)
    date_of_Birth = models.DateField(default=timezone.now)
    mobile_number_regex = RegexValidator(regex="^[0-9]{10,15}$",
                                         message="Entered mobile number isn't in a right format!")
    parent_Mobile_Number = models.CharField(
        validators=[mobile_number_regex], max_length=13, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    profile_Picture = models.FileField(upload_to='profile_pics/')
    address = models.TextField(blank=True)
    course = models.ForeignKey('school_management.Course', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Grade(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=5, decimal_places=2)
    course = models.ForeignKey('school_management.Course', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.student.user.username} - {self.course.name} - {self.value}"
    # student, value, course, datetimecreated


list_of_grates = []


# class AttendanceReport(models.Model):
#     student = models.ForeignKey(Student, on_delete=models.DO_NOTHING)
#     session = models.ForeignKey(Session, on_delete=CASCADE)
#     attendance = models.ForeignKey(Attendance, on_delete=models.CASCADE)
#     status = models.BooleanField(default=False)
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now_add=True)


class Teacher(models.Model):
    name = models.CharField(max_length=255)
    course = models.ManyToManyField('school_management.Course', related_name='teachers')

    # rating = models.DecimalField(max_digits=3, decimal_places=1)
    # teacher_profile_picture = models.FileField(upload_to='profile_pics/')

    def __str__(self):
        return self.name


class WorkDayTime(models.Model):
    WORK_DAYS = (
        (0, 'Luni'),
        (1, 'Marti'),
        (2, 'Miercuri'),
        (3, 'Joi'),
        (4, 'Vineri'),
        (5, 'Sambata'),
        (6, 'Duminica')
    )
    work_day = models.IntegerField(choices=WORK_DAYS)
    time = models.TimeField()


class Course(models.Model):
    name = models.CharField(max_length=255)
    teacher = models.ForeignKey(Teacher, on_delete=models.SET_NULL, null=True, blank=True, related_name='courses')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    work_day_times = models.ManyToManyField(WorkDayTime)

    def __str__(self):
        return self.name


# class Session(models.Model):
#     teacher = models.ForeignKey(Profile, on_delete=models.CASCADE)
#     course = models.ForeignKey(Course, on_delete=models.CASCADE)
#     students = models.ManyToManyField(Student, related_name='sessions')
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#     notes = models.TextField(blank=True)


class Attendance(models.Model):
    student = models.ForeignKey(Profile, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    date = models.DateField()
    type = models.CharField(max_length=50, choices=(
        ('Present', 'Present'),
        ('Absent', 'Absent'),
    ))

    def __str__(self):
        return f"{self.student.user.username} - {self.course.name} - {self.date}"


# class Attendance(models.Model):
#     student = models.ForeignKey(Profile, on_delete=models.CASCADE)
#     course = models.ForeignKey(Course, on_delete=models.CASCADE)
#     date = models.DateField()

# def __str__(self):
#     return f"{self.student.user.username} - {self.course.name} - {self.date}"
# # foreign key Student -> Group


class StudentExtra(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    roll = models.CharField(max_length=10)
    mobile = models.CharField(max_length=40, null=True)
    fee = models.PositiveIntegerField(null=True)
    cl = models.CharField(max_length=10, choices=classes, default='one')
    status = models.BooleanField(default=False)

    @property
    def get_name(self):
        return self.user.first_name + " " + self.user.last_name

    @property
    def get_id(self):
        return self.user.id

    def __str__(self):
        return self.user.first_name
