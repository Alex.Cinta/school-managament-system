"""
URL configuration for school_management_system project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, include

import school_management_system
from . import views
from .views import CreateGradeView, UpdateGradeView, DeleteGradeView, list_of_schedule, dashboard

urlpatterns = [
    path('', views.home, name='homepage'),
    path('list_of_courses/', views.list_of_courses, name='list_of_courses'),
    path('login/', LoginView.as_view(template_name='registration/login.html'), name='login'),
    path('homepage/', views.home, name='homepage'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('record_attendance/', views.record_attendance, name='record-attendance'),
    path('create-grade/', CreateGradeView.as_view(), name='create_grade'),
    path('update-grade/<int:grade_id>/', UpdateGradeView.as_view(), name='update_view'), # folosim <int:grade_id> pentru a reprezenta identificatorul notei pe care vrei sa o actualizezi -> ID-ul
    path('delete-grade/<int:grade_id>/', DeleteGradeView.as_view(), name='delete_view'),
    path('cursuri/', views.ListOfCoursesView.as_view(), name='cursuri'),
    path('profesori/', views.ListOfTeachersView.as_view(), name='list_of_teachers'),
    path('program_saptamanal/', list_of_schedule, name='list_of_schedule'),
    path('dashboard/', dashboard, name='dashboard'),
    path('student-attendance/', views.ListAttendanceView.as_view(), name='student-attendance'),
    path('student-attendance-view/', views.AttendanceView.as_view(), name='student-attendance-view'),

    path('studentlogin/', LoginView.as_view(template_name='registration/studentlogin.html'), name='student_login'),

]
