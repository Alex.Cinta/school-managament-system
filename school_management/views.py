from django.forms import forms
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.utils.regex_helper import Group
from django.views import View

from school_management import models
from school_management.forms import AttendanceForm
from school_management.models import Profile, Course, Student, Attendance, Grade, Teacher, WorkDayTime


# Create your views here.
class ProfileView(View):
    def get(self, request):
        profiles = Profile.objects.all()
        return render(request, 'profile/profile_list.html', {'profiles': profiles})


# class CoursesView(View):
#     def get(self, request):
#         courses = Courses.objects.all()
#         return render(request, 'courses_list.html', {'courses': courses})

class StudentsView(View):
    def get(self, request):
        students = Student.objects.all()
        return render(request, 'students_list.html', {'courses': students})


class ListOfTeachersView(View):
    def get(self, request):
        teachers = Teacher.objects.all()
        return render(request, 'teacher/list_of_teachers.html', {'teachers': teachers})


class CreateGradeView(View):
    def get(self, request):
        return render(request, 'grades/create_grade.html')

    def post(self, request):
        student_id = request.POST.get('student_id')
        course_id = request.POST.get('course_id')
        value = request.POST.get('value')

        grade = Grade.objects.create(student_id=student_id, course_id=course_id, value=value)
        return redirect('grades/list_of_grades')


class UpdateGradeView(View):
    def get(self, request, grade_id):
        grade = Grade.objects.get(pk=grade_id)
        return render(request, 'grades/update_grade.html', {'grade': grade})

    def post(self, request, grade_id):
        grade = Grade.objects.get(pk=grade_id)
        grade.value = request.POST.get('value')
        grade.save()
        return redirect('list_of_grades')


class DeleteGradeView(View):
    def get(self, request, grade_id):
        grade = Grade.objects.get(pk=grade_id)
        return render(request, 'grades/delete_grade.html', {'grade': grade})

    def post(self, request, grade_id):
        grade = Grade.objects.get(pk=grade_id)
        grade.delete()
        return redirect('list_of_grades')


class CreateAttendanceView(View):
    def get(self, request):
        return render(request, 'attendance/create_attendance.html')

    def post(self, request):
        student_id = request.POST.get('student_id')
        course_id = request.POST.get('course_id')
        date = request.POST.get('date')

        attendance = Attendance.objects.create(student_id=student_id, course_id=course_id, date=date)
        return redirect('list_of_attendance')


class UpdateAttendanceView(View):
    def get(self, request, attendance_id):
        attendance = Attendance.objects.get(pk=attendance_id)
        return render(request, 'attendance/update_attendance.html', {'attendance': attendance})

    def post(self, request, attendance_id):
        attendance = Attendance.objects.get(pk=attendance_id)
        attendance.date = request.POST.get('date')
        attendance.save()
        return redirect('list_of_attendance')


class DeleteAttendanceView(View):
    def get(self, request, attendance_id):
        attendance = Attendance.objects.get(pk=attendance_id)
        return render(request, 'attendance/delete_attendance.html', {'attendance': attendance})

    def post(self, request, attendance_id):
        attendance = Attendance.objects.get(pk=attendance_id)
        attendance.delete()
        return redirect('list_of_attendance')


class ListOfCoursesView(View):
    def get(self, request):
        courses = Course.objects.all()
        return render(request, 'courses/list_of_courses.html', {'courses': courses})


class AttendanceView(View):
    def get(self, request):
        attendance = Attendance.objects.all()
        return render(request, 'attendance/student_view_attendance.html', {'attendances': attendance})


class ListAttendanceView(View):
    def get(self, request):
        attendance = Attendance.objects.all()
        return render(request, 'attendance/student_view_attendance_ask_date.html', {'attendance': attendance})


class ScheduleListView(View):
    pass


def list_of_courses(request):
    courses = Course.objects.all()
    return render(request, 'courses/list_of_courses.html', {'courses': courses})


def list_of_teacher(request):
    teachers = Teacher.objects.all()
    return render(request, 'teacher/list_of_teachers.html', {'teachers': teachers})


def home(request):
    return render(request, 'home/homepage.html', {})


def record_attendance(request):
    if request.method == 'POST':
        form = AttendanceForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('list_of_courses')
    else:
        form = AttendanceForm()
    return render(request, 'attendance/record_attendance.html', {'form': form})


def list_of_schedule(request):
    workday_times = WorkDayTime.objects.all()
    work_days = [0, 1, 2, 3, 4, 5, 6]
    work_hours = ['08:00', '09:00', '10:00', '11:00', '12:00', '13:00']

    return render(request, 'schedule/list_of_schedule.html', {
        'workday_times': workday_times,
        'work_days': work_days,
        'work_hours': work_hours
    })


def dashboard(request):
    attendance = Attendance
    contact = Student.mobile_number_regex
    return render(request, 'dashboard/student_dashboard.html', {'dashboards': dashboard})


def student_signup_view(request):
    form1 = forms.StudentUserForm()
    form2 = forms.StudentExtraForm()
    mydict = {'form1': form1, 'form2': form2}
    if request.method == 'POST':
        form1 = forms.StudentUserForm(request.POST)
        form2 = forms.StudentExtraForm(request.POST)
        if form1.is_valid() and form2.is_valid():
            user = form1.save()
            user.set_password(user.password)
            user.save()
            f2 = form2.save(commit=False)
            f2.user = user
            user2 = f2.save()

            my_student_group = Group.objects.get_or_create(name='STUDENT')
            my_student_group[0].user_set.add(user)

        return HttpResponseRedirect('studentlogin')
    return render(request, 'registration/studentsignup.html', context=mydict)
